
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import LSTM, GRU, Dense
from xgboost import XGBRegressor
from sklearn.svm import SVR
import matplotlib.pyplot as plt

# Load data
df = pd.read_csv('GOOG.csv')
df['Date'] = pd.to_datetime(df['Date'])
df.set_index('Date', inplace=True)

# Check if DataFrame is empty
if df.empty:
    print("DataFrame is empty. Please ensure 'GOOG.csv' contains valid data.")
        exit()

        # Check if DataFrame index is in datetime format
        if not isinstance(df.index, pd.DatetimeIndex):
            print("DataFrame index is not in datetime format. Please ensure 'Date' column is properly formatted.")
                exit()

                # Normalize data
                scaler = MinMaxScaler(feature_range=(0, 1))
                data_scaled = scaler.fit_transform(df[['Close']].values.reshape(-1, 1))

                # Define look-back window size
                look_back = 20

                # Function to create sequences
                def create_sequences(data, look_back):
                    X, y = [], []
                        for i in range(len(data) - look_back):
                                X.append(data[i:(i + look_back), 0])
                                        y.append(data[i + look_back, 0])
                                            return np.array(X), np.array(y)

                                            # Create sequences
                                            X, y = create_sequences(data_scaled, look_back)

                                            # Split data into training and testing sets
                                            train_size = int(len(X) * 0.8)
                                            X_train, X_test = X[:train_size], X[train_size:]
                                            y_train, y_test = y[:train_size], y[train_size:]

                                            # LSTM model
                                            lstm_model = Sequential()
                                            lstm_model.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
                                            lstm_model.add(LSTM(units=50))
                                            lstm_model.add(Dense(units=1))
                                            lstm_model.compile(optimizer='adam', loss='mean_squared_error')
                                            lstm_model.fit(X_train, y_train)

                                            # GRU model
                                            gru_model = Sequential()
                                            gru_model.add(GRU(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
                                            gru_model.add(GRU(units=50))
                                            gru_model.add(Dense(units=1))
                                            gru_model.compile(optimizer='adam', loss='mean_squared_error')
                                            gru_model.fit(X_train, y_train)

                                            # XGBoost model
                                            xgb_model = XGBRegressor()
                                            xgb_model.fit(X_train, y_train)

                                            # SVM model
                                            svm_model = SVR(kernel='rbf')
                                            svm_model.fit(X_train, y_train)

                                            # Make predictions
                                            lstm_predictions = lstm_model.predict(X_test)
                                            gru_predictions = gru_model.predict(X_test)
                                            xgb_predictions = xgb_model.predict(X_test)
                                            svm_predictions = svm_model.predict(X_test)

                                            # Inverse transform predictions
                                            lstm_predictions = scaler.inverse_transform(lstm_predictions)
                                            gru_predictions = scaler.inverse_transform(gru_predictions)
                                            xgb_predictions = scaler.inverse_transform(xgb_predictions.reshape(-1, 1))
                                            svm_predictions = scaler.inverse_transform(svm_predictions.reshape(-1, 1))
                                            y_test = scaler.inverse_transform(y_test.reshape(-1, 1))

                                            # Calculate RMSE
                                            lstm_rmse = np.sqrt(mean_squared_error(y_test, lstm_predictions))
                                            gru_rmse = np.sqrt(mean_squared_error(y_test, gru_predictions))
                                            xgb_rmse = np.sqrt(mean_squared_error(y_test, xgb_predictions))
                                            svm_rmse = np.sqrt(mean_squared_error(y_test, svm_predictions))

                                            print(f'LSTM RMSE: {lstm_rmse}')
                                            print(f'GRU RMSE: {gru_rmse}')
                                            print(f'XGBoost RMSE: {xgb_rmse}')
                                            print(f'SVM RMSE: {svm_rmse}')

                                            # Plot actual vs predicted close prices for LSTM
                                            plt.figure(figsize=(12, 6))
                                            plt.plot(df.index[-len(lstm_predictions):], y_test, label='Actual Close Price', color='blue')
                                            plt.plot(df.index[-len(lstm_predictions):], lstm_predictions, label='Predicted Close Price (LSTM)', color='red')
                                            plt.title('Stock Price Prediction using LSTM')
                                            plt.xlabel('Date')
                                            plt.ylabel('Close Price')
                                            plt.legend()
                                            plt.show()

                                            # Plot actual vs predicted close prices for GRU
                                            plt.figure(figsize=(12, 6))
                                            plt.plot(df.index[-len(gru_predictions):], y_test, label='Actual Close Price', color='blue')
                                            plt.plot(df.index[-len(gru_predictions):], gru_predictions, label='Predicted Close Price (GRU)', color='red')
                                            plt.title('Stock Price Prediction using GRU')
                                            plt.xlabel('Date')
                                            plt.ylabel('Close Price')
                                            plt.legend()
                                            plt.show()

                                            # Plot actual vs predicted close prices for XGBoost
                                            plt.figure(figsize=(12, 6))
                                            plt.plot(df.index[-len(xgb_predictions):], y_test, label='Actual Close Price', color='blue')
                                            plt.plot(df.index[-len(xgb_predictions):], xgb_predictions, label='Predicted Close Price (XGBoost)', color='red')
                                            plt.title('Stock Price Prediction using XGBoost')
                                            plt.xlabel('Date')
                                            plt.ylabel('Close Price')
                                            plt.legend()
                                            plt.show()

                                            # Plot actual vs predicted close prices for SVM
                                            plt.figure(figsize=(12, 6))
                                            plt.plot(df.index[-len(svm_predictions):], y_test, label='Actual Close Price', color='blue')
                                            plt.plot(df.index[-len(svm_predictions):], svm_predictions, label='Predicted Close Price (SVM)', color='red')
                                            plt.title('Stock Price Prediction using SVM')
                                            plt.xlabel('Date')
                                            plt.ylabel('Close Price')
                                            plt.legend()
                                            plt.show()

                                            